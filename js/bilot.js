/**
 * Binder scripts
 */
var userId = "";
var binderData = [];
var likedImages = [];
var imageCount = 10;
var binderPoints = 0;
var token = "";
$(document).ready(function () {
	$.each($('.page'), function(index, value) { 
		$(this).hide();
	});
	if(getUrlParameter("game") == "1" || getUrlParameter("game") == "2" || getUrlParameter("game") == "3"){
		show("Page2", "");
		moveProgress(0);
		binderPoints = $("#tinderslide").attr("data-points");
		$("#Page3").load("page3.html");
	}else {
		show("Page1", "");
	}
	
	// create user id
	var d = new Date();
	userId = d.getTime();
});

var IDLE_TIMEOUT = 2; //minutes
var _idleSecondsTimer = null;
var _idleSecondsCounter = 0;

document.onclick = function() {
    _idleSecondsCounter = 0;
};

document.onmousemove = function() {
    _idleSecondsCounter = 0;
};

document.onkeypress = function() {
    _idleSecondsCounter = 0;
};

document.onswipe = function() {
    _idleSecondsCounter = 0;
};

_idleSecondsTimer = window.setInterval(CheckIdleTime, 60000);

function CheckIdleTime() {
     _idleSecondsCounter++;
    if (getUrlParameter("ipad") == "true" && _idleSecondsCounter >= IDLE_TIMEOUT) {
        window.clearInterval(_idleSecondsTimer);
        window.location.reload();
    }
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function show(shown, hidden) {
	if(shown == "Page1" || shown == "Page3"){
		$(".masthead").hide();
	} else {
		$(".masthead").show();
	}

	document.getElementById(shown).style.display='block';
	if(hidden != ""){
		document.getElementById(hidden).style.display='none';
	}
	return false;
}

function showBinder(number) {
	window.location.href= "binder"+number+".html?game="+number;
}

function toStartPage() {
	window.location.href="index.html";
}

function setBinderData(item, liked) {
	if(liked){
		likedImages.push(item);
	}
	var itemNumber = item.attr("data-id");
	var itemSet = item.attr("data-set");
	var itemPlusPoints = item.attr("data-plus");
	var itemMinusPoints = item.attr("data-minus");
	var d = new Date();
	sendData(new BinderLikes(userId,itemNumber,itemSet,liked?1:0));
	binderData.push({"item":item,"liked":liked});
	setBinderPoints(liked, liked?itemPlusPoints:itemMinusPoints);
	moveProgress(binderData.length);
	
	$("button.dislike").removeAttr("disabled")
	$("button.like").removeAttr("disabled");
}

function setBinderPoints(liked, points) {
	if(liked){
		binderPoints = parseInt(binderPoints) + parseInt(points);
	}else{
		binderPoints = parseInt(binderPoints) - parseInt(points);
	}
	
	imageCount--;
	if(imageCount < 1){
		var diagnosePercentage = 0;
		if(binderPoints < 2){
			$("#diagnose1").show();
			diagnosePercentage = 20;
		} else if(binderPoints < 4){
			$("#diagnose2").show();
			diagnosePercentage = 40;
		} else if(binderPoints < 6){
			$("#diagnose3").show();
			diagnosePercentage = 60;
		} else if(binderPoints < 8){
			$("#diagnose4").show();
			diagnosePercentage = 80;
		} else{
			$("#diagnose5").show();
			diagnosePercentage = 100;
		}	
		show("Page3", "Page2");
		
		var stepSize = 50;
		setTimeout((function () {
			var filler = document.getElementById("filler"),
				percentage = 0;
			return function progress() {
				filler.style.height = percentage + "%";
				percentage += 1;
				if (percentage <= diagnosePercentage) {
					setTimeout(progress, stepSize);
				}
			}

		}()), stepSize);
	}
}

function moveProgress(count) {
	var value = count + 1  + ' / 10';
	document.getElementById("progress-label").innerHTML = value;
}

function submitEmail() {
	var email = $(".chatEmail").val();
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(re.test(email)){
		//if(token == ""){
			//getToken("email", new BinderEmail(userId,email));
		//}
		sendEmail(new BinderContacts(userId,email));
		$(".chatText").text(email);
		$(".chatEmail").hide();
		$(".submitEmail").hide();
	}else{
		$(".chatEmail").addClass("error");
	}
}

function BinderLikes(userId,itemNumber,itemSet,liked) {
	this.session = userId + "";
	this.game = itemSet;
	this.question = itemNumber;
	this.swipelike = liked ? "true" : "false";
}
function BinderDiagnoses(userId,itemNumber,itemSet) {
	this.session = userId + "";
	this.game = itemSet;
	this.diagnose = itemNumber;
}
function BinderContacts(userId,email) {
	this.email = email;
	this.session = userId + "";
}

function sendEmail(object) {
	var json = JSON.stringify(object);
	$.ajax({
		method: 'POST',
		url: 'http://10.100.113.253:3000/contacts',
		data: json,
		contentType: 'application/json'
	});
}

function sendDiagnoses(object) {
	var json = JSON.stringify(object);
	$.ajax({
		method: 'POST',
		url: 'http://10.100.113.253:3000/diagnoses',
		data: json,
		contentType: 'application/json'
	});
}

function sendData(object) {
	var json = JSON.stringify(object);
	$.ajax({
		method: 'POST',
		url: 'http://10.100.113.253:3000/likes',
		data: json,
		contentType: 'application/json'
	});
}


/**
 * jTinder initialization
 */
$("#tinderslide").jTinder({
	// dislike callback
	onDislike: function (item) {
		setBinderData(item, false);
	},
	// like callback
	onLike: function (item) {
		setBinderData(item, true);
	},
	animationRevertSpeed: 200,
	animationSpeed: 400,
	threshold: 1,
	likeSelector: '.like',
	dislikeSelector: '.dislike'
});

/**
 * Set button action to trigger jTinder like & dislike.
 */
$('.actions .like, .actions .dislike').click(function(e){
	e.preventDefault();
	$(this).attr("disabled", "disabled");
	$("#tinderslide").jTinder($(this).attr('class'));
});